/**
 * 倒计时目标日期
 */
const targetDate = new Date('June 22, 2024 00:00:00 GMT+0800');
/**
 * 当前倒计时单位索引
 */
let countdownUnitIndex = 0;
/**
 * 每个单位对应的换算方法
 */
const millisecondsPerUnit = {
    '个月': 1000 * 60 * 60 * 24 * getDaysInMonth(), // 调用函数获取当前月的天数
    '个星期': 1000 * 60 * 60 * 24 * 7,
    '天': 1000 * 60 * 60 * 24,
    '小时': 1000 * 60 * 60,
};
/**
 * 倒计时结束
 */
const countdownStartedMessage = '中考已开始';
/**
 * 倒计时元素
 */
const countdownElement = $('#countdown');
/**
 * 一言元素
 */
const oneWord = $('#one-world');

/**
 * 获取当前月份的实际天数
 */
function getDaysInMonth() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1; // 月份从 0 开始，所以需要加 1
    return new Date(year, month, 0).getDate();
}
/**
 * 更新倒计时显示
 */
function updateCountdown() {
    /**
     * 当前时间
     */
    let currentDate = new Date();
    /**
     * 计算时差
     */
    let timeDifference = targetDate - currentDate;
    /**
     * 时间单位
     */
    let unit = Object.keys(millisecondsPerUnit)[countdownUnitIndex];
    let timeValue;
    // 根据当前单位计算时间值
    timeValue = Math.floor(timeDifference / millisecondsPerUnit[unit]);
    // 更新倒计时内容
    countdownElement.text(`${timeValue} ${unit}`);
    // 倒计时结束
    if (timeDifference <= 0) {
        countdownElement.text(countdownStartedMessage);
    }
}
/**
 * 切换倒计时单位
 */
function toggleCountdownUnit() {
    countdownUnitIndex = (countdownUnitIndex + 1) % Object.keys(millisecondsPerUnit).length;
    updateCountdown();
}
setInterval(updateCountdown, 2400000);
updateCountdown();
countdownElement.on('click', toggleCountdownUnit);


// 每日一言
$.getJSON('https://api.vvhan.com/api/dailyEnglish/')
    .done(function (data) {
        oneWord.html(`${data.data.zh} <br> ${data.data.en}`);
    })
    .fail(function (error) {
        oneWord.html(error);
    });

/**
 * 生成日历
 */
new Calendar({
    parent: 'calendar',
})

/**
 * 日历可拖动
 */
$("#calendar").draggable({
    containment: "parent", // 限制在父容器中拖动
    scroll: false, // 禁用滚动条滚动
    drag: function (event, ui) {
        // 限制元素不超出屏幕
        var containment = $(".draggable").parent();
        var leftPos = ui.position.left;
        var topPos = ui.position.top;

        if (leftPos < 0) {
            ui.position.left = 0;
        }
        if (topPos < 0) {
            ui.position.top = 0;
        }
        if (leftPos + $(this).width() > containment.width()) {
            ui.position.left = containment.width() - $(this).width();
        }
        if (topPos + $(this).height() > containment.height()) {
            ui.position.top = containment.height() - $(this).height();
        }
    }
});

/**
 * 自定义背景图片
 */
$('#background-input .submit').click(function () {
    const fileInput = $('#fileInput')[0];
    // 如果选择了文件
    if (fileInput.files.length > 0) {
        const file = fileInput.files[0];
        const reader = new FileReader();
        reader.onload = function (event) {
            const base64Image = event.target.result;
            $('body').css('background-image', `url('${base64Image}')`);
        };
        reader.readAsDataURL(file);
    } else {
        // 如果未选择文件，则检查手动输入的图片地址
        $('#selectFile').text(`未选择文件`);
    }
});
$('#background-input #fileInput').change(function () {
    const fileName = $(this)[0].files[0].name;
    $('#selectFile').text(`已选择的文件：${fileName}`);
});
$('#background-input .reset').click(function () {
    // 重置背景图片为默认值
    $('body').css('background-image', 'none');
    $('#fileInput').val('');
    $('input[name="imageUrl"]').val('');

    $('#selectFile').text(``);
});